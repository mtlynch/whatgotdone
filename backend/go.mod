module github.com/mtlynch/whatgotdone/backend

go 1.16

require (
	cloud.google.com/go/storage v1.14.0
	github.com/clbanning/mxj v1.8.4 // indirect
	github.com/disintegration/imaging v1.6.2
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/gorilla/csrf v1.7.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/ikeikeikeike/go-sitemap-generator/v2 v2.0.2
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/workpail/userkit-go v0.0.0-20180527213510-29d105cd872b
	golang.org/x/mod v0.4.2 // indirect
	google.golang.org/api v0.44.0
	google.golang.org/genproto v0.0.0-20210406143921-e86de6bf7a46 // indirect
	google.golang.org/grpc v1.37.0 // indirect
)
